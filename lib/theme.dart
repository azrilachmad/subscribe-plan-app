import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color whiteColor = Color(0xffFFFFFF);
TextStyle titleTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight : FontWeight.w700,
  fontSize : 26,
);

Color greyColor = Color(0xff8997B8);
TextStyle subTitleTextStyle = GoogleFonts.poppins(
  color: greyColor,
  fontWeight: FontWeight.w300,
  fontSize: 16,
);

TextStyle planTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.w500,
  fontSize: 14,
);

TextStyle descTextStyle = GoogleFonts.poppins(
  color: greyColor,
  fontWeight: FontWeight.w300,
  fontSize: 12,
);

TextStyle priceTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.w700,
  fontSize: 18,
);

TextStyle buttonTextStyle = GoogleFonts.poppins(
  color: whiteColor,
  fontWeight: FontWeight.w500,
  fontSize: 16,
);